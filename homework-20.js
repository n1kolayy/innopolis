"use strict";

// Упражнение 1 

for (let a = 1; a <= 20; a++)
    if (a % 2 == 0)
        console.log(a)

// Упражнение 2
let sum = 0
while (true) {
    let a = +prompt('Введите первое число');
    if (!a) alert('Вы ввели не число')
    if (!a) break
    let b = +prompt('Введите второе число');
    if (!b) alert('Вы ввели не число')
    if (!b) break
    let c = +prompt('Введите третье число');
    if (!c) alert('Вы ввели не число')
    if (!c) break


    sum = a + b + c
    alert('Получилось: ' + sum)
    break;
}

// Упражнение 3

function getNameOfMonth(n) {

    if (n === 0) return 'Январь';
    if (n === 1) return 'Февраль';
    if (n === 2) return 'Март';
    if (n === 3) return 'Апрель';
    if (n === 4) return 'Май';
    if (n === 5) return 'Июнь';
    if (n === 6) return 'Июль';
    if (n === 7) return 'Август';
    if (n === 8) return 'Сентябрь';
    if (n === 9) return 'Октябрь';
    if (n === 10) return 'Ноябрь';
    if (n === 11) return 'Декабрь';

}


for (let n = 0; n < 12; n++) {
    const month = getNameOfMonth(n);
    if (n === 9) continue;
    console.log(month);
}
