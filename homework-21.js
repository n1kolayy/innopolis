"use strict";

// Упражнение 1

/** 
 * Описание для функции isEmpty(user)
 * @param {for} перебирает свойства объекта
 * @return {return} возвращаем false, как только встречаем свойство
 * @return {return} возвращаем true, если свойств нет
 */

let user = {};
function isEmpty(user) {
    for (let key in user) {
        return false;
    }
    return true;
}

console.log(isEmpty(user))

user.name = 'Kolya'
console.log(isEmpty(user))

// Упражнение 3

/**
 * Описание для функции raiseSalary(percent)
 * @param {for} перебирает свойства объекта 
 * @discription прибавляем % повышения зп 
 * @discription округляем до целого числа в меньшую сторону
 * @discription выводим в консоль ключ + пробел + увеличенную ЗП
 * @param {for} перебирает свойства объекта 
 * @discription складываем все данные которые получились в результате повыщения ЗП 
 * @discription выводим в консоль сумму 
 */

let salaries = {
    John: 100000,
    Ann: 160000,
    Pete: 130000,
};

let perzent = +prompt('Введите % повышения зп', ' ')
let sum = 0;
let newSalaries = 0;
function raiseSalary(percent) {
    for (let key in salaries) {
        salaries[key] = salaries[key] + (salaries[key] / 100) * perzent;
        newSalaries = Math.floor(salaries[key]);
        console.log(key + ' ' + newSalaries);
    
}
    for (let key in salaries) {
        sum += newSalaries;
    }
    console.log(sum)
}
    raiseSalary()
